from attrdict import AttrDict
import json
import os.path
import os
import tempfile


class JsonDict(object):
    def __init__(self,persistence=None):
        # TODO: either filename or file-like object
        # for now, just filename
        self._persistence = persistence
        self._wrapped = dict()

    def __len__(self):
        self._reread()
        return len(self._wrapped)

    def keys(self):
        self._reread()
        return self._wrapped.keys()

    def values(self):
        self._reread()
        return self._wrapped.values()

    def __setitem__(self, key, item):
        self._wrapped[key] = item
        self._commit()
        return item

    def __delitem__(self, key):
        del self._wrapped[key]
        self._commit()

    def __getitem__(self, key):
        self._reread()
        if key in self._wrapped:
            return self._wrapped[key]
        else:
            return None

    def _commit(self):
        serialized = json.dumps(self._wrapped)
        atomic_write(self._persistence, serialized)

    def _reread(self):
        with open(self._persistence, 'rb') as fd:
            self._wrapped = json.load(fd)

#def PersistentAppAttrDict(appname):
    #configfilename = appdirs.user_data_dir(appname,appname) + "appconfig.json"
    #open

def atomic_write(fn,content):
    filedir = os.path.dirname(os.path.abspath(fn))
    if not os.path.exists(filedir):
        os.makedirs(filedir)
    (tempfd, tempfn) = tempfile.mkstemp(dir=filedir)
    tempfh = os.fdopen(tempfd, 'w')
    tempfh.write(content)
    tempfh.close()
    os.rename(tempfn,fn)
