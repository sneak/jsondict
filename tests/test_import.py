# -*- coding: utf-8 -*-
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

from jsondict import JsonDict  # noqa


# pylint: disable=unused-import,unused-variable
def test_import():
    _ = JsonDict()  # noqa
