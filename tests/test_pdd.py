# -*- coding: utf-8 -*-
from jsondict import JsonDict, atomic_write
import json
import unittest


class Testcases(unittest.TestCase):

    def test_atomic_write(self):
        ts = "this is my test string"
        tfn = '/tmp/pad_test.txt'
        atomic_write(tfn, ts)
        with open(tfn, 'r') as fh:
            self.assertEqual(fh.read(), ts)

    def test_persistence(self):
        tfn = '/tmp/pad_test.json'
        mypdd = JsonDict(persistence=tfn)
        mypdd['testattr'] = 'loldongs'

        with open(tfn, 'r') as fh:
            x = json.load(fh)

        self.assertEqual(x['testattr'], 'loldongs')

        del mypdd['testattr']

        self.assertEqual(mypdd['testattr'], None)

    def test_read(self):
        tfn = '/tmp/pad_test.json'
        mypdd = JsonDict(persistence=tfn)
        mypdd['testattr'] = 'loldongs'

        with open(tfn, 'w') as fh:
            json.dump({'testattr': 'notloldongs'}, fh)

        self.assertNotEqual(mypdd['testattr'], 'loldongs')

        self.assertEqual(mypdd['nonexist'], None)
