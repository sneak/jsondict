#!/usr/bin/env python3
from pipenv.project import Project
from pipenv.utils import convert_deps_to_pip
from setuptools import setup, find_packages
import sys

pfile = Project(chdir=False).parsed_pipfile
requirements = convert_deps_to_pip(pfile['packages'], r=False)
test_requirements = convert_deps_to_pip(pfile['dev-packages'], r=False)

setup(
    name='jsondict',
    version='0.0.1',
    description='dumb json persistence for a dict with defaults',
    long_description=open('README.markdown','r').read(),
    keywords=['persistence', 'json', 'datastructure'],
    license='MIT',
    url='https://github.com/sneak/jsondict',
    maintainer='Jeffrey Paul',
    maintainer_email='sneak@sneak.berlin',
    packages=find_packages(),
    setup_requires=[
        'pytest-runner',
        'pipenv',
    ],
    tests_require=test_requirements,
    install_requires=requirements,
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Development Status :: 4 - Beta'
    ])
