# jsondict

This is a python package called jsondict that wraps a dict to
provide dumb json file backed persistence.

It also returns a default of `None` for missing keys instead of KeyError,
because an exception for an undefined key is annoying, so it's sort of like
a defaultdict.

It's not general purpose, and it's not for large amounts of data.  It's
just sort of like a standard dictionary that you don't need to worry about
reading/saving to disk.  It's naive and inefficient but perfect for
application configs and whatnot.  Don't be silly and put more than a few
kilobytes in this, the whole file is written out every update and the whole
file is re-read and re-parsed every read.

# Installation

```
pip3 install --upgrade pipenv
git clone https://github.com/sneak/jsondict.git
cd jsondict
pipenv install --three .
```

# Other Info

Right now it's only tested on python3.  Python2 support is desired.

# Author

Jeffrey Paul <sneak@sneak.berlin>

# See Also

* [attrdict](https://github.com/bcj/AttrDict)

# License

MIT
